# csv 모듈을 임포트합니다.
# -*- coding: utf-8 -*-
import re
import urllib.request

from bs4 import BeautifulSoup
import csv
import codecs
import sys


from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter


SLACK_TOKEN = "xoxb-689227472356-691998787108-b4DXM5fTNML7HVDAu7SdIHF6"
SLACK_SIGNING_SECRET = "1f40045e6f9e7f05d3130da8d3593e82"




filename = 'local_money.csv'
def get_place_info(filename):
   with open(filename, 'r', encoding='utf-8') as file:
       # ',' 기호로 분리된 CSV 파일을 처리하세요..
       reader = csv.reader(file, delimiter=',')

       places=[]
       for row in reader:
           place = {
               'store': row[0],
               'region': row[1],
               'add1': row[2],
               'add2': row[3]
           }
           places.append(place)

       return places


# 가게 이름 입력 시, 해당 가게의 주소를 리턴
def get_addr(text):
   places = get_place_info(filename)

   temp_split=text.split()
   every = []

   if len(temp_split) == 2:
       for dic in places:
           if temp_split[1] in dic["store"]:
               every.append("[" + dic["store"] + "]" + ' : ' + dic["add1"] + dic["add2"])

       for dic1 in places:
           if temp_split[1] in dic1["region"]:
               every.append("[" + dic1["store"] + "]" + ' : ' + dic1["add1"] + dic1["add2"])

   elif len(temp_split) == 3:
       for dic2 in places:
           if temp_split[1] in dic2['region']:
               if temp_split[2] in dic2['store']:
                   every.append("[" + dic2["store"] + "]" + ' : ' + dic2["add1"] + dic2["add2"])

   if not every:
       return "해당 정보를 찾을 수 없습니다ㅠㅠ"

   messages = u'\n'.join(every)
   return messages


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
   channel = event_data["event"]["channel"]
   text = event_data["event"]["text"]

   temp = text.split()
   #print(temp)

   if "도와줘" == temp[1]:
       slack_web_client.chat_postMessage(
           channel=channel,
           text="안녕하세요. Show Me The Place입니다.\n"
                "화성시 내 지역화폐 사용처를 안내드리고 있습니다.\n"
                "키워드를 입력해주세요 [지역(읍,면,동)  or  상호명  or  지역 + 상호명]"
       )
   else:
       slack_web_client.chat_postMessage(
           channel=channel,
           text=get_addr(text)
       )

@app.route("/", methods=["GET"])
def index():
   return "<h1>Server is ready.</h1>"



if __name__ == '__main__':
    app.run('127.0.0.1', port=5050)